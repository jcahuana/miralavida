<?php  
include("libs/Setup.php");
include("libs/utils.php");

$Setup = new Setup();
$txtNombres = (isset($_POST['txt_nombres']))? utf8_decode($_POST['txt_nombres']): '';
$txtApellidos = (isset($_POST['txt_apellidos']))? utf8_decode($_POST['txt_apellidos']): '';
$txtEmail = (isset($_POST['txt_email']))? utf8_decode($_POST['txt_email']): '';
$txtDni = (isset($_POST['txt_dni']))? $_POST['txt_dni']: '';
$txtTelefono = (isset($_POST['txt_telefono']))? $_POST['txt_telefono']: '';
$fechaRegistro = date('Y-m-d H:i:s');
$fechaDia = date('Y-m-d');
$fechaHora = date('H-i-s');
$fechaCompromiso = $fechaDia."_".$fechaHora;//Igual a la fecha de registro pero en formato diferente

$existsDni = existsDni($txtDni);
if($existsDni == false){
	//1. Crear imagen
	$nombreImagen = crearImagenCompromiso($txtDni,$txtNombres,$txtApellidos,$fechaCompromiso);

	//2. Guardar registro
	$registro = guardarRegistro($txtNombres,$txtApellidos,$txtEmail,$txtDni,$txtTelefono,$nombreImagen,$fechaRegistro);
	
	$arrayReturn = array(
	"registro" => "ok",
	"urlimagen" => "".$nombreImagen.""
	);

	if($registro == true){
		$json = json_encode($arrayReturn);
		echo $json;
	}
}else{

	$arrayReturn = array(
	"registro" => "no",
	"urlimagen" => ""
	);

	$json = json_encode($arrayReturn);
	echo $json;
}








?>