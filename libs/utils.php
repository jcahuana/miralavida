<?php 
//Verifica si existe el usuario
function existsDni($dni){
	$q = "SELECT dni FROM registro WHERE dni = '".$dni."'";
	$r = mysql_query($q);
	$num_rows = mysql_num_rows($r);

	if($num_rows > 0){
		return true;
	}else{
		return false;
	}
}
function guardarRegistro($_txtNombres,$_txtApellidos,$_txtEmail,$_txtDni,$_txtTelefono,$_urlImgCompromiso,$_fechaRegistro){

	$q = "INSERT INTO registro(nombres,apellidos,email,dni,telefono,img_compromiso,fecha_registro) VALUES('$_txtNombres','$_txtApellidos','$_txtEmail','$_txtDni','$_txtTelefono','$_urlImgCompromiso','$_fechaRegistro')";
	$r = mysql_query($q);
	$id = mysql_insert_id();
	
	if($id != 0){
		return true;
	}else{
		return false;
	}
}

//Crear imagen para postear en facebook
function crearImagenCompromiso($dni,$nombres,$apellidos,$fechaRegistro){

	//Carpeta donde se guardan las imágenes generadas
	$rutaSalida = "compromisos/";

	//Imagen de Fondo
	$pathImgFondo = "images/img-base-compromiso.jpg";
	$imagenPost = imagecreatefromjpeg($pathImgFondo);//Imagen Creada con GD

	//Función que construye las imágenes
	function imagettftextSp($image, $size, $angle, $x, $y, $color, $font, $text, $spacing = 0){
	    if ($spacing == 0)
	    {
	        imagettftext($image, $size, $angle, $x, $y, $color, $font, $text);
	    }
	    else
	    {
	        $temp_x = $x;
	        $temp_y = $y;
	        for ($i = 0; $i < strlen($text); $i++)
	        {
	            imagettftext($image, $size, $angle, $temp_x, $temp_y, $color, $font, $text[$i]);
	            $bbox = imagettfbbox($size, 0, $font, $text[$i]);
	            $temp_x += cos(deg2rad($angle)) * ($spacing + ($bbox[2] - $bbox[0]));
	            $temp_y -= sin(deg2rad($angle)) * ($spacing + ($bbox[2] - $bbox[0]));
	        }
	    }
	}

	//Formato de textos
	$colorAzul = imagecolorallocate($imagenPost, 0, 43, 87);
	$fuente = "fuentes/helveticaneueltstdbd.ttf";
	
	//1. Nombres: buscamos si tiene espacio
	$find   = ' ';
	$nombreUpper = mb_strtoupper($nombres);
	$espacioNombre = strpos($nombreUpper, $find);

	if($espacioNombre === false){
		$nombre1 = $nombreUpper;
	}else{
		$nombreSeparado = explode(" ",$nombreUpper);
		$nombre1 = $nombreSeparado[0];
	}

	//2. Apellidos: buscamos si tiene espacio
	$apellidoUpper = mb_strtoupper($apellidos);
	$espacioApellido = strpos($apellidoUpper, $find);

	if($espacioApellido === false){
		$apellido1 = $apellidoUpper;
	}else{
		$apellidoSeparado = explode(" ",$apellidoUpper);
		$apellido1 = $apellidoSeparado[0];
	}

	//3. Nombre a imprimir en la imagen
	$nombreFinal = $nombre1." ".$apellido1;

	//Calcula donde debe comenzar el titulo ejm: "Yo, JUAN CAHUANA"
	$txtPara = "Yo, ";
	$nombreImprimir = "Yo, ".$nombreFinal.",";
	$bboxTitulo = imagettfbbox(25, 0, $fuente, $nombreImprimir);
	$xTitulo = (650 - ($bboxTitulo[2]-$bboxTitulo[0])) / 2;

	imagettftextSp($imagenPost, 25, 0, $xTitulo, 117, $colorAzul, $fuente, $nombreImprimir, 0);

	//Ruta y nombre para guardar la imagen final
	$nomImgSalida = $dni."_".$fechaRegistro;
	$nomCompletoImgSalida = $rutaSalida.$nomImgSalida.".jpg";

	//Crea la imagen de salida
	imagejpeg($imagenPost,"".$nomCompletoImgSalida."",100);

	//Elimina la imagen de la carpeta temporal
	imagedestroy($imagenPost);

	$nombre_imagen = $nomImgSalida.".jpg";
	return $nombre_imagen;
}
?>