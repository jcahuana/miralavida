<?php
include("libs/utils.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login</title>
<!-- Estilos -->
<link href='https://fonts.googleapis.com/css?family=Raleway:400,500' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/zebra/zebra_dialog.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />

<!-- JQuery $ JQuery UI -->
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<script type="text/javascript" src="js/zebra_dialog.js"></script>
</head>
<body id="bd_login">
	<div id="wrapper">
      <div id="main">
         <form action="login.php" method="POST" name="form_login" id="form_login">
            <div id="content-login">
               <h2>Login</h2>
               <input type="text" id="input_user" name="input_user" placeholder="Usuario"/>
               <input type="password" id="input_pass" name="input_pass" placeholder="Contraseña"/>
               <button class="btn btn-primary">Ingresar</button>
            </div>      
         </form>    
      </div>
	</div>
	<!-- endof wrapper -->
<script type="text/javascript" src="js/login.js"></script>
</body>
</html>