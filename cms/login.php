<?php 
session_start();
include("../libs/Setup.php");
require("libs/utils.php");
$Setup = new Setup();

if(isset($_POST['input_user']) && isset($_POST['input_pass'])){

	$user = $_POST['input_user'];
	$pass = $_POST['input_pass'];

	$login = userLogin($user,$pass);

	if($login == true){
		$_SESSION['usuario'] = $user;
		$_SESSION['contrasena'] = $pass;
		header("Location: reporte.php");
		exit();

	}elseif ($login == false) {
		header("Location: form-login.php");
		exit();
	}

}else{
	header("Location: form-login.php");
	exit();
}

?>