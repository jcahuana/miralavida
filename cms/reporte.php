<?php
session_start();

//Validar Logeo
if(isset($_SESSION['usuario'])){
   $usuario = $_SESSION['usuario'];
}else{
   header("Location: form-login.php");
   exit();
}

//Fechas por defecto del buscador
$fecha_hoy = date("d/m/Y");
$fecha_inicio = date('d-m-Y',strtotime('-30 day')); 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Administrador de Consultas de Formulario</title>
<!-- Estilos -->
<link href='https://fonts.googleapis.com/css?family=Raleway:400,500' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />

<!-- JQuery $ JQuery UI -->
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<!-- Zebra Dialog -->
<script type="text/javascript" src="js/zebra_dialog.js"></script>
<!-- Boostrap -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</head>
<body id="bd_pendiente">
	<div id="wrapper">
		<div id="header">
            <h1>REGISTRO MIRALAVIDA.PE</h1>
            <div id="content-bienvenida">Bienvenido: <?php echo $usuario; ?></div>
            <div id="content-logout"><a href="logout.php"><button class="btn btn-danger">Salir</button></a></div>
        </div>
         
         <div id="main">
         <!-- Formulario de Filtros -->
         <form action="genera-reporte.php" method="post" name="form_reporte" id="form_reporte">
         <input type="hidden" name="input_validate" value="on" />
        	<div id="sidebar">
            <div id="cont_filtro_fecha">
               <!-- <h2>Fecha :</h2>
               <div class="controls">
                  <div class="input-prepend">
                  <span class="add-on"><i class="icon-calendar"></i></span><input type="text" name="filtro_fecha" id="filtro_fecha" <?php echo "value='".$fecha_inicio." - ".$fecha_hoy."'" ?>>
                  </div>
               </div>
               <span class="label label-info">Por defecto los últimos 30 días</span> -->
            </div>
            <div id="cont_filtro_tipo">
               <!-- <h2>Tipo :</h2>
               <select name="filtro_motivo" id="filtro_motivo">
                  <option value="">- Seleccionar -</option>
                  <option value="1">Pedir Cotización</option>
                  <option value="2">Quejas y Reclamos</option>
                  <option value="3">Consulta</option>
               </select> -->
            </div>
            <div id="cont_btngenerar">
            <!-- <button class="btn" type="button"><i class="icon-search"></i> Buscar</button> -->
            </div>
        	</div>
        	<div id="contenido">
            <button id="btn_descargar_reporte"class="btn btn-large btn-success" type="submit">Descargar</button>
        	</div>
        	<div class="clearfix"></div>
         </form> 
         <!-- endof Formulario de Filtros-->
         </div>
         <!-- endof main -->
        <div id="footer"></div>
	</div>
	<!-- endof wrapper -->
<script type="text/javascript" src="js/funciones.js"></script>
   </body>
</html>