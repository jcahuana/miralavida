$(document).ready(function(){

	//Filtro Rango de Fecha
	$('#filtro_fecha').daterangepicker({
		format: "dd/MM/yyyy",
		minDate: '12/09/2012',
		locale: {
            applyLabel: 'Listo',
            fromLabel: 'Desde',
            toLabel: 'Hasta'
        }
    },null);

    $("#form_reporte").validate({
	   rules:{
	        filtro_fecha: "required",
	        filtro_motivo: "required"
	    },
	    messages: {
	        filtro_fecha: "",
	        filtro_motivo: ""
	    },
	    submitHandler: function(form){
		    form.submit();
		}
	});


	function enviarFormulario(i_proceso){
		$("input#i_proceso").attr("value",i_proceso);
		document.form_procesar.submit();

		//Enviar por ajax
		/*var url = $("#form_procesar").attr('action'); 
		var datos = $("#form_procesar").serialize();
		$.post(url,datos,recibir); 
		return false; */
	}
	function recibir(datos){
	}
	function mensajeAlert(){
		$.Zebra_Dialog('<div class="zebra_alertbox"><h2>Mensaje</h2>Seleccione al menos una imagen.</div>');	
	}

});