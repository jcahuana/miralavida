<?php
ini_set("memory_limit","100M");
include("classes/PHPExcel.php");

/*********************************** LOGIN DE USUARIO ****************************************/
function userLogin($user,$pass){
	$q = "SELECT usuario,contrasena FROM login";
	$r = mysql_query($q);
	while($fila = mysql_fetch_array($r)){
		
		$userDB = $fila['usuario'];
		$passDB = $fila['contrasena'];

		//Comparar las claves y usuarios
		if($user == $userDB && $pass == $passDB){
			return true;
		}else{
			return false;
		}	
	}
}

/************************************** REPORTE DE JAMBOREE 2013 ****************************************/
function reporteMiralavida(){

	$nombre_archivo="Participantes_Site_Miralavida";
	$consulta = mysql_query("SELECT nombres,apellidos,email,dni,telefono,img_compromiso,fecha_registro FROM registro WHERE dni<>'44808124' AND email<>'juan@gmail.com' AND email<>'test@gmail.com' AND email<>'hola@gmail.com' AND email<>'dfd@asdsd.com' AND email<>'user@gmail.com' AND  email<>'a@sda.ca' AND nombres<>'jbjkbkjbkb' AND nombres<>''");
	
	// Crea un nuevo objeto PHPExcel
	$objPHPExcel = new PHPExcel();

	$num_filas = mysql_num_rows($consulta);
	
	//Formato String para los DNI
	//$ultima_fila_temp = $num_filas+1;
	//$objPHPExcel->getActiveSheet()->getStyle('C1:C'.$ultima_fila_temp)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue("A1", "Nº");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B1", "Nombres");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C1", "Apellidos");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue("D1", "Email");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue("E1", "Dni");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue("F1", "Teléfono");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue("G1", "Imagen");
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue("H1", "Fecha de Registro");

	$fila_excel=2;
	$num_orden=1;
	while($resultado = mysql_fetch_array($consulta)){

		$fecha_formateada=date("d-m-Y H:i:s",strtotime($resultado['fecha_registro']));
	
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$fila_excel, $num_orden);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$fila_excel, utf8_encode($resultado['nombres']));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C".$fila_excel, utf8_encode($resultado['apellidos']));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("D".$fila_excel, utf8_encode($resultado['email']));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("E".$fila_excel, $resultado['dni']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("F".$fila_excel, $resultado['telefono']);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("G".$fila_excel, utf8_encode($resultado['img_compromiso']));
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("H".$fila_excel, $fecha_formateada);
		$fila_excel++;
		$num_orden++;
	}

	/*********************************** Estilos ***********************************/
	//Tamaño de texto por defecto y Fuente
	$objPHPExcel->getDefaultStyle()->getFont()->setSize(11);
	$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
	//Color de fondo de titulos
	$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF00AEEF');
	//Negrita para titulos
	$styleArrayNegrita = array(
		'font' => array(
		'bold' => true,
		'color' => array('argb' => 'FFFFFFFF'),
		)
	);
	$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->applyFromArray($styleArrayNegrita);
	//Color de fondos intercalados
	$num_fila_bg=2;
	for($fila_bg=1; $fila_bg<$num_filas+1;$fila_bg++){

		if($num_fila_bg%2 == 0){
			//Sila fila es numero par
			$objPHPExcel->getActiveSheet()->getStyle('A'.$num_fila_bg.':H'.$num_fila_bg)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFFFF');

		}else{

			//Si la fila es numero impar
			$objPHPExcel->getActiveSheet()->getStyle('A'.$num_fila_bg.':H'.$num_fila_bg)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFF5F5F5');

		}
		$num_fila_bg++;
	}
	//Ultima fila Excel
	$ultima_fila=$num_filas+1;

	//Bordes de Celdas
	$styleArray = array(
	  'borders' => array(
	    'allborders' => array(
	      'style' => PHPExcel_Style_Border::BORDER_THIN,
		  'color' => array('argb' => 'FFCCCCCC'),
	    )
	  )
	);

	$objPHPExcel->getActiveSheet()->getStyle('A1:H'.$ultima_fila)->applyFromArray($styleArray);
	unset($styleArray);

	/*********************************** Alineanciones ***********************************/
	//Centrar Cabeceras
	$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getAlignment()->sethorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	//Centrado vertical de todas las celda
	$objPHPExcel->getActiveSheet()->getStyle('A1:'.'H'.$ultima_fila)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

	//Centrado Horizontal: Nº Orden
	$objPHPExcel->getActiveSheet()->getStyle('A2:'.'A'.$ultima_fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	//Centrado horizontal: DNI
	$objPHPExcel->getActiveSheet()->getStyle('E2:'.'E'.$ultima_fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	//Centrado horizontal: Teléfono
	$objPHPExcel->getActiveSheet()->getStyle('F2:'.'F'.$ultima_fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	//Centrado horizontal: Fecha
	$objPHPExcel->getActiveSheet()->getStyle('H2:'.'H'.$ultima_fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	
	//Dimensiones Ancho y Alto *************************

	//Altura de filas por defecto
	$objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(23);
	//Ancho de columnas
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);//Numero de Orden
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);//Nombres
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);//Apellidos
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);//Correo
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);//Dni
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(17);//Telefono
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(40);//Telefono
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);//Fecha

	//Zoom de la hoja de calculo
	$objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(80);

	//Nombre de Hoja
	$objPHPExcel->getActiveSheet()->setTitle('Reporte de Usuarios');
	//Establecer la hoja activa, para que cuando muestre el documento se muestre ésta
	$objPHPExcel->setActiveSheetIndex(0);
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

	header ('Content-type: application/x-msexcel;charset=iso-8859-1');
	header("Content-Disposition: attachment; filename=".$nombre_archivo.".xls");
	header("Pragma: no-cache");
	header("Expires: 0");

	$objWriter->save('php://output');
	exit;

}
?>