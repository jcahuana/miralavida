<!DOCTYPE html>
<html lang="es-PE" xmlns:fb="http://ogp.me/ns/fb#">
<head>
	<meta charset="utf-8"/>
	<title>DALE UNA NUEVA MIRADA A LA VIDA</title>
	<meta property="fb:app_id" content="466363000095479" /> 
	<meta property="og:type" content="website" />
	<meta property="og:title" content="DALE UNA NUEVA MIRADA A LA VIDA" />
	<meta property="og:description" content="Comienza a vivir al máximo, ingresa a www.miralavida.pe y descubre el NEW CERATO 2013." />
	<meta property="og:url" content="http://www.miralavida.pe" />
	<meta property="og:image" content="http://miralavida.pe/images/thumb-facebook.png" />

	<link rel="stylesheet" href="css/normalize.css">
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/mediaelementplayer.css">
	<link rel="stylesheet" href="css/jquery.jtweetsanywhere-1.3.1.css">
	<link rel="stylesheet" href="css/fuentes.css">
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/menu-principal.css">
	<link rel="stylesheet" href="css/footer.css">
	<link rel="stylesheet" href="css/jquery.fancybox.css">
	<link rel="stylesheet" type="text/css" href="css/calendar.css" />
	<link rel="stylesheet" type="text/css" href="css/calendar-custom.css" />
	
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>
<body>	
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-27680990-4']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
	<div id='fb-root'></div>
	<div class="overlay-calendario"></div>
	<!-- <div id="bg-full"></div>		 -->
	<div id="wrapper">
		<div id="header">
			<div id="logo"><img src="images/logo-kia.png" alt=""></div>
			<nav id="menu-principal">
				<ul>
					<li><a class="btn-principal" id="btn_viral" href="#">Dale una nueva <span class="menu-bold">mirada a la vida</span></a>
						<!-- Submenu Video Intro -->
						<ul class="submenu-viral">
							<li>
								<p>Mira nuestro manifiesto y <span>compártelo</span>.</p>
								<div class="btn-video-viral"><a id="intro" class="modal-videos" href="#content-video">&nbsp;</a></div>
							</li>
						</ul>
					</li>
					<li><a class="btn-principal" id="btn_acciones" href="#">Participa y <span class="menu-bold">gana</span></a>
						<!-- Submenu Videos -->
						<ul class="submenu-videos">
							<li>
								<p>Elige que te gustaría hacer, <span>dale click</span> y <span>participa</span>.</p>
								<div class="group-videos">
									<div id="btn-puenting">
										<a class="modal-videos" id="puenting" href="#content-video"><img src="images/btn-puenting.png" alt=""></a>
									</div>
									<div id="btn-parapente">
										<a class="modal-videos" id="parapente" href="#content-video"><img src="images/btn-parapente.png" alt=""></a>
									</div>
									<div id="btn-paddle" class="last-child">
										<a class="modal-videos" id="paddle" href="#content-video"><img src="images/btn-paddle.png" alt=""></a>
									</div>
								</div>
							</li>
						</ul>
					</li>
					<li class="last-child"><a class="btn-principal" id="btn_conocecerato" href="http://kia.com.pe/microsites/newcerato/" target="_blank">Conoce al <span class="menu-bold">All New Cerato</span></a></li>
				</ul>
			</nav>	
		</div>	
		<!-- endof header-->

		<div id="main">	
			<div id="txt-lema">
			<p>¿Estás haciendo lo que <span>realmente te gusta en este momento</span>?</p>
			</div>
			<div id="txt-precio"><img src="images/txt-precio.png" alt=""></div>

			<div id="btn-miracalendario">
				<img src="images/btn-vercalendario.png" alt="">
			</div>
			<!-- <div id="auto-composicion"><img src="images/auto.png" alt=""></div> -->
			<div id="content-tabs-social">
				<ul id="menu">
					<li class="active"><a href="#contentTwitterFeed"><img src="images/ico-twitter.png" alt=""></a></li>
					<li><a href="#contentFacebookFeed"><img src="images/ico-facebook.png" alt=""></a></li>
				</ul>
				<div class="content-twitter" id="contentTwitterFeed">
					<div id="content-tweets-scroll"></div>
				</div>
				<div class="content-facebook" id="contentFacebookFeed">
					<fb:activity site="https://www.kia.com.pe" app_id="466363000095479" width="230" height="170" header="false" font="arial" recommendations="false"></fb:activity>
				</div>
			</div>
			<div id="social-terminos">
				<div id="buttons-social">
					<div class="btnlike">
						<div class="fb-like" data-href="http://miralavida.pe/" data-send="false" data-layout="button_count" data-width="120" data-show-faces="false" data-font="tahoma"></div>
					</div>
					<div class="btnplus">
						<g:plusone size="medium" lang="es-PE"></g:plusone> 
					</div>
					<div class="btntweet">
						<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://miralavida.pe/" data-lang="es">Twittear</a>
					</div>
					<div class="btnpintit">
						<a data-pin-config="above" href="//pinterest.com/pin/create/button/" data-pin-do="buttonBookmark" ><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" /></a>
					</div>
				</div>
				
				<div id="terminos"><p>*Bono de US$ 2.000 ó S/. 5,300 válido para autos con año de fabricación 2012. Precio final incluido bono para versión MT-LX, US$ 17,990. Bono de US$ 1.000 ó S/. 2,650 válido para autos de año de fabricación 2013. Precio final incluido bono para versión MT-LX, US$ 18,990. Fotos referenciales con accesorios opcionales. Promoción válida hasta el 15/05/2013. Stock mínimo 20 unidades. Equipamiento varía de acuerdo a versión. Cambio de equipamiento sin previo aviso. T.C.R. S/. 2.65 válido a la fecha de 05/04/13. Precios incluyen IGV. Promoción no válida para Pandero EAFC S.A. Garantía de 5 años válida o de 100 mil kilómetros lo que suceda primero. Precio no incluye costo de traslado a provincia.</p></div>
			</div>
			
			<div class="copyright"><p>Copyright (c) | 2013 Kia Motor Corp. Todos los Derechos Reservados</p></div>
		</div>
		<!-- endof main -->

		<!-- Video Intro -->
		<div id="content-video">
			<div class="overlay-formulario"></div>
			<div id="video-contador">
				<div class="txt-contator">10</div>
				<div class="btn-si"><img src="images/btn-si.png" alt=""></div>
			</div>
			<!-- Formulario de Registro -->
			<div id="box-formulario">
				<div class="btn-close-lightbox-form"><img src="images/btn-close-lightbox.png" alt=""></div>
				<div id="overlay-loader-form"><div id="loader-img"><img src="images/preloader.gif" alt=""></div></div>
				<form action="#" id="form-registro" name="form-registro" method="post">
					<!-- Nombres -->
					<div class="input"><input type="text" name="txt_nombres" id="txt_nombres"/></div>

					<!-- Apellidos -->
					<div class="input"><input type="text" name="txt_apellidos" id="txt_apellidos"/></div>

					<!-- Email -->
					<div class="input"><input type="text" name="txt_email" id="txt_email"/></div>

					<!-- Dni -->
					<div class="input"><input type="text" name="txt_dni" id="txt_dni"/></div>

					<!-- Teléfono -->
					<div class="input"><input type="text" name="txt_telefono" id="txt_telefono"/></div>
					<input type="image" src="images/btn-submit.png" id="btn-submit"/>
				</form>
			</div>

			<!-- Venatana de Compromiso -->
			<div id="compromiso">
				<div id="box-preloder-compartir"><div class="preloader-compartir"></div></div>
				<div id="box-image-compromiso"><img src="compromisos/compartir.jpg" alt=""></div>
				<div id="box-social">
					<div class="btn-share-facebook"><img src="images/btn-share-facebook.png" alt=""></div>
					<div class="btn-share-twitter">
						<div class="botontwitterhide">
							<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://miralavida.pe/" data-text="Me comprometo a darle una nueva mirada a la vida hoy. Ingresa y comprométete en" data-lang="es" data-hashtags="miralavidaCerato">Twittear</a>
						</div>
						<img src="images/btn-share-twitter.png" alt="">
					</div>
				</div>
			</div>

			<video id="reproductor-video" width="927px" height="521px" poster="videos/intro/poster.jpg" controls="false" preload="none">
			    <!-- MP4 for Safari, IE9, iPhone, iPad, Android, and Windows Phone 7 -->
			    <source type="video/mp4" src="videos/intro/intro.mp4" />
			    <!-- WebM/VP8 for Firefox4, Opera, and Chrome -->
			    <source type="video/webm" src="videos/intro/intro.webm" />
			    <!-- Ogg/Vorbis for older Firefox and Opera versions -->
			    <source type="video/ogg" src="videos/intro/intro.ogg" />
			    <source type="video/youtube" src="http://www.youtu.be/nOEw9iiopwI" />
			    <!-- Flash fallback for non-HTML5 browsers without JavaScript -->
			    <object width="320" height="240" type="application/x-shockwave-flash" data="swf/flashmediaelement.swf">
			        <param name="movie" value="swf/flashmediaelement.swf" />
			        <param name="flashvars" value="controls=true&file=videos/intro/intro.mp4" />
			        <!-- Image as a last resort -->
			        <img src="videos/intro/mivideo.jpg" width="320" height="240" title="No video playback capabilities" />
			    </object>
			</video>
		</div>
		
		<div id="lightbox-calendario">
			<div class="btn-close-lightbox"><img src="images/btn-close-lightbox.png" alt=""></div>
					
			<div class="custom-calendar-wrap">
				<div id="custom-inner" class="custom-inner">
					<div class="content-custom-header">
						<div class="custom-header clearfix">
							<div id="nav">
								<span id="custom-prev" class="custom-prev"></span>
								<span id="custom-next" class="custom-next"></span>
							</div>
							<h2 id="custom-month" class="custom-month"></h2>
							<h3 id="custom-year" class="custom-year"></h3>
						</div>	
					</div>
					<div id="calendar" class="fc-calendar-container"></div>
				</div>
			</div>
			<!-- <div id="mask-eventos"></div> -->
			<!-- endof mask -->
				
		</div>
		<!-- endof lightbox-calendario-->
	
	</div>
	
	<!-- Plugins para botones sociales -->
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
	<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>

	<script src="js/fb.js"></script>
	<script src="js/jquery.min.js"></script>
	<script src="js/modernizr.custom.63321.js"></script>
	<script src="js/mediaelement-and-player.min.js"></script>
	<script src="js/jquery.validate.min.js"></script>
	<script src="js/jquery.fancybox.pack.js"></script>
	<script src="js/jquery.fancybox-media.js?v=1.0.5"></script>
	<script src="js/jquery.jtweetsanywhere-1.3.1.js"></script>
	<script src="js/jquery.tabify.js"></script>
	<script src="js/widget-social.js"></script>
	<script type="text/javascript" src="js/jquery.calendario.js"></script>
	<script type="text/javascript" src="js/data-calendario.js"></script>
	<script type="text/javascript" src="js/calendario.js"></script>
	<script src="js/main.js"></script>

</body>
</html>
