$(document).on("ready",initWidgetSocial);
function initWidgetSocial(){
	$('#content-tweets-scroll').jTweetsAnywhere({
        username: 'KiaPeruOficial',
        count: 3,
        showTweetFeed: {
        	showProfileImages: true,
	        paging: {
	            mode: 'endless-scroll'
	        }
	    },
	    onDataRequestHandler: function(stats) {
	        if (stats.dataRequestCount < 2) {
	            return true;
	        }
	        else {
	        }
	    }
    });
    $('#content-tabs-social #menu').tabify();
}
