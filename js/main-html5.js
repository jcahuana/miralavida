console.log('html5');
var playerVideo;
var flagContador = false;
var videoContador = $("#video-contador");
var intervalCont;
var txtFinalMensajeFacebook;
var videoFinal;
var IndicadorVideo;
var IE8;

var MV = {
	Init : function(){

		//Comprobar si es IE8
		IE8 = MV.isIE8();

		//Menú principal
		$('#menu-principal ul li').not('.last-child').on('mouseenter',MV.OverMenu);
		$('#menu-principal ul li').not('.last-child').on('mouseleave',MV.OutMenu);

		//Vetanas Modales
		MV.CallModal();

		//Validar Formulario
		MV.ValidarFormulario();

		//Tabs Sociales
		MV.PreventHashtag();

		MV.CallLightboxCalendario();

	},
	isIE8 : function(){
	
		var is_ie = navigator.userAgent.toLowerCase().indexOf('msie ') > -1;
		if (is_ie) {
	        var posicion = navigator.userAgent.toLowerCase().lastIndexOf('msie ');
	        var ver_ie = navigator.userAgent.toLowerCase().substring(posicion+5, posicion+8);
    	}

    	if(ver_ie == "8.0"){
    		return true;
    	}else{
    		return false;
    	}

	},
	OverMenu : function(){
		
		$('#header').stop().animate({
			height : 300
		});

		//Mostrar el Submenu
		var nombreBoton = $(this).children('a.btn-principal').attr('id');
		if(nombreBoton == "btn_viral"){
			$('.submenu-viral').fadeTo(500,1);
		}

		if(nombreBoton == "btn_acciones"){
			$('.submenu-videos').fadeTo(500,1);
		}
	},
	OutMenu : function(){
		
		$('#header').stop().animate({
			height : 60
		});

		var nombreBoton = $(this).children('a.btn-principal').attr('id');

		//Ocultar el Submenu
		if(nombreBoton == "btn_viral"){
			$('.submenu-viral').fadeTo(100,0).hide();
		}

		if(nombreBoton == "btn_acciones"){
			$('.submenu-videos').fadeTo(100,0).hide();
		}
	},
	PreventHashtag : function(){
		$("#content-tabs-social #menu li a").on("click", function(e){
			
			$("#content-tabs-social #menu li").removeClass("active")
			var liParent = $(this).parent().toggleClass("active");

			var hash = $(this).attr("href");
			if(hash == "#contentTwitterFeed-tab"){
				$("#contentTwitterFeed").show();
				$("#contentFacebookFeed").hide();
			}
			if(hash == "#contentFacebookFeed-tab"){
				$("#contentFacebookFeed").show();
				$("#contentTwitterFeed").hide();
			}
			e.preventDefault();
		});
	},
	SourceVideo : function(nombre){
		var source;

		//Video Intro
		if(nombre == "intro"){
			source = "intro";
		}
		//Video Puenting
		if(nombre == "puenting"){
			source = "puenting_no";
		}
		//Video Parapente
		if(nombre == "parapente"){
			source = "parapente_no";
		}
		//Video Paddle
		if(nombre == "paddle"){
			source = "paddle_no";
		}
		return source;
	},
	CallModal : function(){

		$(".modal-videos").fancybox({
			fitToView	: true,
			autoSize	: true,
			autoHeight  : true,
			autoCenter  : true,
			autoResize  : true,
			aspectRatio : true,
			scrolling   : 'no',
			closeClick	: false,
			openEffect	: 'fade',
    		closeEffect	: 'fade',
		    helpers:  {
		        title:  null,
		        media : {},
		        overlay : {closeClick: false}
		    },
		    onUpdate : function(){
		    	
		    	var wVid = $('.fancybox-inner').width();
		    	var hVid = wVid*(0.5625);
		    	$("#reproductor-video").width(wVid);
		    	$("#reproductor-video").height(hVid);
		    	
		    	$(".mejs-overlay, .mejs-layerm, .mejs-overlay-play").width(wVid);
		    	$(".mejs-overlay, .mejs-layerm, .mejs-overlay-play").height(hVid);
		    	$(".mejs-container, .svg mejs-video").height(hVid);
		    },
		    beforeShow : function(){
		    	var nomVideo = this.group[0].element.context.id;
		    	IndicadorVideo = nomVideo;

			
				switch(nomVideo){
					case "puenting":
					videoFinal = "puenting";
					txtFinalMensajeFacebook = "por un salto puenting.";
					break;

					case "parapente":
					videoFinal = "parapente";
					txtFinalMensajeFacebook = "por un salto en parapente.";
					break;

					case "paddle":
					videoFinal = "paddle";
					txtFinalMensajeFacebook = "en paddle.";
					break;
				}

				//HTML5
				if(IE8 == false || IE8 === undefined){

		    	$("#reproductor-video").attr("poster","videos/"+nomVideo+"/poster.jpg");
				playerVideo = new MediaElementPlayer('#reproductor-video',{

					features: ['playpause','progress','current','duration','tracks','volume'],
					startVolume: 0.3,
					poster : 'videos/'+nomVideo+'/poster.jpg',
					success: function(media, domNode) {
						
				        media.addEventListener('play', function() {
				        	//console.log('play');
				        
				        }, false);
				 
				       	media.addEventListener('timeupdate', function() {
				        	
				        	if(IndicadorVideo != "intro"){

				        		if(media.currentTime > 15){
									
									if(flagContador == false){

										flagContador = true;
										MV.MostrarCronometro();
									}
								
								}
				        	}
				        	
				        }, false);
				 		
				        media.addEventListener('pause', function() {
				        	//console.log('pause');
				        }, false);
				       
				       	//Cuando el video termina
				        media.addEventListener('ended', function() {
				        }, false);
		    		}
				});
				
				var nomSource = MV.SourceVideo(nomVideo);

					playerVideo.setSrc([
						{ src:'videos/'+nomVideo+'/'+nomSource+'.mp4', type:'video/mp4' },
						{ src:'videos/'+nomVideo+'/'+nomSource+'.webm', type:'video/webm' },
						{ src:'videos/'+nomVideo+'/'+nomSource+'.ogg', type:'video/ogg' }
					]);
					playerVideo.media.load();
					playerVideo.play();
					

				}else{
					
					//Método para IE8
					
				}
				
				
  			},
  			beforeClose : function(){

  				//HTML5
  				if(IE8 == false || IE8 === undefined){
  				playerVideo.pause();
  				}
  				
  			},
	        afterClose: function(){
	        	clearInterval(intervalCont);	
	        	flagContador = false;
				$(".txt-contator").text(10);
	        	videoContador.hide();
	        	
	        }
		});
	},
	MostrarVideoFinal : function(){
		var path,name;
		switch(videoFinal){
			case "puenting":
			path = "puenting";
			name = "puenting_si";
			break;

			case "parapente":
			path = "parapente";
			name = "parapente_si";
			break;

			case "paddle":
			path = "paddle";
			name = "paddle_si";
			break;
		}
		
		playerVideo.setSrc([
			{ src:'videos/'+path+'/'+name+'.mp4', type:'video/mp4' },
			{ src:'videos/'+path+'/'+name+'.webm', type:'video/webm' },
			{ src:'videos/'+path+'/'+name+'.ogg', type:'video/ogg' }
		]);
				
		playerVideo.media.load();
		playerVideo.play();
	},
	MostrarCronometro : function(){
		
		//Mostrar el timer
		videoContador.fadeTo(400,1);

		//Evento que llama al formulario
		$("#video-contador .btn-si").on("click",function(){
			playerVideo.pause();
			clearInterval(intervalCont);
			MV.ShowFormulario();
		});

		intervalCont = setInterval(cronometro,1000);
		function cronometro(){
									
			var selectorCont = $(".txt-contator");
			if(selectorCont.text()>0){

				selectorCont.text(selectorCont.text()-1);

			}else{
				
				//1. Para el contador
				clearInterval(intervalCont);

				//2. Desaparece el box contador
				$("#video-contador").fadeTo(500,0,function(){
					$(this).hide();
				});

				//3.Desactiva listener para llamar al formulario
				$("#video-contador .btn-si").off("click");
				
			}
		}
	},
	ShowFormulario : function(){
		//Mostrar el formulario
		$(".overlay-formulario").show();
		$("#box-formulario").show();
		$(".btn-close-lightbox-form").show();

		$("#video-contador .btn-si").off("click");
		$("#video-contador").hide();

		//Botón cerrar el popup del formulario
		$(".btn-close-lightbox-form").on("click",cerrarLightboxFormulario);
		

		function cerrarLightboxFormulario(){
			$('body').removeClass('noscroll');
			$(".overlay-formulario").hide();
			$("#box-formulario").hide();

			playerVideo.play();
		}
	},
	ValidarFormulario : function(){
		$("#form-registro").validate({
	        rules: {
	            txt_nombres: "required",
	            txt_apellidos: "required",
	            txt_email: {
		            required: true,
		            email: true
		        },
	            txt_dni: {
		            required: true,
		            digits: true,
		            rangelength: [8, 8]
		        },
	            txt_telefono: {
		            required: true,
		            digits: true
		        }
	        },
	        messages: {
		        txt_nombres: "",
		        txt_apellidos: "",
		        txt_email: {
		            required: "",
		            email: ""
		        },
		        txt_dni: {
		        	required: "",
		            digits: "",
		            rangelength: ""
		        },
		        txt_telefono: {
		            required: "",
		            digits: ""
		        }
		    },
	        submitHandler: function(form){
	        	//Aparece el loader del formulario
	        	$("#overlay-loader-form").fadeTo(400,1);

	        	//Se quita el botón cerrar del formulario
	        	$(".btn-close-lightbox-form").off("click");
	        	$(".btn-close-lightbox-form").hide();

				
	            var formulario = form;
	            $.ajax({
	                url: 'guardar-registro.php',
	                dataType: 'json',
	                type: 'post',
	                data: $(formulario).serialize(),
	                success: function(data){
	                	
	                	if(data.registro == "ok"){
	                		var urlImagen = "compromisos/"+data.urlimagen;
	                		urlImgUploadFacebook = urlImagen;
	                		$("#box-image-compromiso img").attr("src",urlImagen);
	                		$("#box-formulario").hide();
	                		$("#overlay-loader-form").fadeTo(400,0,function(){
	                			$(this).hide();
	                		});
	                		$('#form-registro').each(function(){this.reset();});

	                		MV.ShowCompartir();

	                	}else if(data.registro == "no"){
	                		$("#overlay-loader-form").fadeTo(400,0,function(){
	                			$(this).hide();
	                		});
	                	}
	                }
	            });         
	        }
	    });
	},
	ShowCompartir : function(){
		$("#compromiso").fadeTo(400,1);

		$(".btn-share-facebook img").on("click", MV.CompartirFacebook);
		$(".btn-share-twitter img").on("click", MV.CompartirTwitter);
	},
	CompartirFacebook : function(){
		App = {
			LoginFacebook: function(){
				FB.login(function(response){
					App.VerificaLogin();
				},{scope: 'email,user_photos,photo_upload,publish_stream'});
			},
			VerificaLogin: function(){
				
				FB.getLoginStatus(function(response){
					if(response.status == "connected"){

						App.UploadPhoto();
					}else{
						App.LoginFacebook();
					}
				});
			},
			UploadPhoto: function(){

				//Mostrar el preloader
				$("#box-preloder-compartir").show();

				//Nombre de usuario
				var nombreUsuario;
				FB.api('/me', function(response) {
					var nombreUsuario = response.name;
					var mensajeFacebook = nombreUsuario+' se ha comprometido a darle una nueva mirada a la vida hoy y está participando '+txtFinalMensajeFacebook+". Ingresa a http://miralavida.pe y comprométete";
					var imgURL="http://miralavida.pe/"+urlImgUploadFacebook;//change with your external photo url
					
					FB.api('/photos', 'post', {
					    message:mensajeFacebook,
					    url:imgURL        
					}, function(response){

					    if (!response || response.error) {
					      
					    } else {
					    	
					    	//Cambiar el loader por un check
					    	$(".preloader-compartir").css("background-position","-20px 0");
					    	//Desaparece, opciones compartir y overlay

					    	$("#box-preloder-compartir").delay(2000).hide(0,function(){
					    		$("#compromiso").hide(0, function(){
					    			$(".preloader-compartir").css("background-position","0 0");
					    			$(".overlay-formulario").fadeTo(1000,0,function(){
					    				$(this).hide();
					    			});

					    			//Mostrar el video Final
					    			MV.MostrarVideoFinal();
					    				
					    		});
					    	
					    	});
					    }
					});
	            });
				
			}
		}

		//Verifica login para subir foto
		App.VerificaLogin();
	},
	CallLightboxCalendario : function(){
		$("#btn-miracalendario img").on("click",function(){
			$('body').addClass('noscroll');
			$(".overlay-calendario").show();
			$("#lightbox-calendario").show();

			$(".btn-close-lightbox").on("click",cerrarLightbox);
		});

		function cerrarLightbox(){
			$('body').removeClass('noscroll');
			$(".overlay-calendario").hide();
			$("#lightbox-calendario").hide();
		}
	}

} 


MV.Init();