
function showEventos(){
   	
   	var codropsEvents = new Object();
   	$.ajax({
		type: "GET",
		url: "eventos.xml",
		dataType: "xml",
		async: false,
		success: function(xml) {
			

			$(xml).find("evento").each(function(){
				var numEventos = $(this).children().size();
				var fecha = $(this).attr('fecha');
				
				anchoGaleria = numEventos*565;

				codropsEvents[fecha] = '<div class="galeria-eventos" style="width:'+anchoGaleria+'px">';
				
				if(numEventos>1){
					
					
					$(this).find('data').each(function(){
						codropsEvents[fecha] += '<div class="bloque-evento">';
						var titulo = $(this).find('titulo').text();
						var imagen = $(this).find('imagen').text();
						var lugar = $(this).find('lugar').text();
						var descripcion = $(this).find('descripcion').text();
						var enlace = $(this).find('enlace').text();

						
						codropsEvents[fecha] += '<h4>'+titulo+'</h4>';
						codropsEvents[fecha] +='<img src="photos/calendario/'+imagen+'">';
						codropsEvents[fecha] +='<div id="descripcion-evento">';
						codropsEvents[fecha] +='<h5>'+lugar+'</h5>';
						codropsEvents[fecha] +='<div class="content-desc">';
						codropsEvents[fecha] +='<p class="descripcion">'+descripcion+'</p>';
						codropsEvents[fecha] +='</div>';
						codropsEvents[fecha] +='<p class="info">Para más información sobre esta emocionante actividad, ingresa a:</p>';
						codropsEvents[fecha] +='<a href="'+enlace+'" target="_blank">'+enlace+'</a>';
						codropsEvents[fecha] +='</div>';
						codropsEvents[fecha] +='</div>';//endof bloque-evento
						
					});
				}else{
					$(this).find('data').each(function(){

						codropsEvents[fecha] = '<div class="bloque-evento">';
						var titulo = $(this).find('titulo').text();
						var imagen = $(this).find('imagen').text();
						var lugar = $(this).find('lugar').text();
						var descripcion = $(this).find('descripcion').text();
						var enlace = $(this).find('enlace').text();

						codropsEvents[fecha] += '<h4>'+titulo+'</h4>';
						codropsEvents[fecha] +='<img src="photos/calendario/'+imagen+'" class="2">';
						codropsEvents[fecha] +='<div id="descripcion-evento">';
						codropsEvents[fecha] +='<h5>'+lugar+'</h5>';
						codropsEvents[fecha] +='<div class="content-desc">';
						codropsEvents[fecha] +='<p class="descripcion">'+descripcion+'</p>';
						codropsEvents[fecha] +='</div>';
						codropsEvents[fecha] +='<p class="info">Para más información sobre esta emocionante actividad, ingresa a:</p>';
						codropsEvents[fecha] +='<a href="'+enlace+'" target="_blank">'+enlace+'</a>';
						codropsEvents[fecha] +='</div>';
						codropsEvents[fecha] +='</div>';//endof bloque-evento	
					});	
				}

				if(numEventos>1){
					codropsEvents[fecha] += '<div class="clearfix"></div>';//endof galeria-eventos
					
				}
				
				
				if(numEventos>1){
					codropsEvents[fecha] += '</div>';//endof galeria-eventos	
					codropsEvents[fecha] += '<div class="nav-galeria-eventos"><ul>';
					for(var i=0; i<numEventos;i++){
						var pos = -(i*565)
						if(i==0){
							codropsEvents[fecha] += '<li class="active" onclick="navEventos(this);" rel="'+pos+'"></li>';
						}else{
							codropsEvents[fecha] += '<li onclick="navEventos(this);" rel="'+pos+'"></li>';
						}
						
					}
					codropsEvents[fecha] += '</ul></div>';
					
					
				}
				
				
				
			});
		}
	});	
	return codropsEvents;
	
    
}
function navEventos(li){
	$(".nav-galeria-eventos li").removeClass("active");
	$(li).addClass("active");

	var posDestino = $(li).attr("rel");
	
	var content = $(li).parent().parent().parent();

	var galeria = content.children(".galeria-eventos");
	galeria.animate({
		left: posDestino
	},500);
}

